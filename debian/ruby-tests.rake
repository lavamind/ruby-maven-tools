require 'rake/testtask'
Rake::TestTask.new(:default) do |test|
  test.test_files = FileList["spec/**/*_spec.rb"]
  test.verbose = false
  test.warning = false
end
